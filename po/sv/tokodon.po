# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the tokodon package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: tokodon\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:47+0000\n"
"PO-Revision-Date: 2022-05-21 11:59+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luna Jernberg,Stefan Asserhäll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "droidbittin@gmail.com,stefan.asserhall@bredband.net"

#: account.cpp:836
#, kde-format
msgid "Could not follow account"
msgstr "Kunde inte följa konto"

#: account.cpp:837
#, kde-format
msgid "Could not unfollow account"
msgstr "Kunde inte sluta följa konto"

#: account.cpp:838
#, kde-format
msgid "Could not block account"
msgstr "Kunde inte blockera konto"

#: account.cpp:839
#, kde-format
msgid "Could not unblock account"
msgstr "Kunde inte sluta blockera konto"

#: account.cpp:840
#, kde-format
msgid "Could not mute account"
msgstr "Kunde inte tysta konto"

#: account.cpp:841
#, kde-format
msgid "Could not unmute account"
msgstr "Kunde inte sluta tysta konto"

#: account.cpp:842
#, kde-format
msgid "Could not feature account"
msgstr "Kunde inte presentera konto"

#: account.cpp:843
#, kde-format
msgid "Could not unfeature account"
msgstr "Kunde inte sluta presentera konto"

#: account.cpp:844
#, kde-format
msgid "Could not edit note about an account"
msgstr "Kunde inte redigera anmärkning om ett konto"

#: accountmodel.cpp:68
#, kde-format
msgid "Loading"
msgstr "Läser in"

#: content/ui/AccountInfo.qml:84
#, kde-format
msgid "Requested"
msgstr "Begärt"

#: content/ui/AccountInfo.qml:87
#, kde-format
msgid "Following"
msgstr "Följer"

#: content/ui/AccountInfo.qml:89
#, kde-format
msgid "Follow"
msgstr "Följ"

#: content/ui/AccountInfo.qml:114
#, kde-format
msgid "Stop notifying me when %1 posts"
msgstr "Sluta underrätta mig när %1 skickar ett inlägg"

#: content/ui/AccountInfo.qml:116
#, kde-format
msgid "Notify me when %1 posts"
msgstr "Underrätta mig när %1 skickar ett inlägg"

#: content/ui/AccountInfo.qml:132
#, kde-format
msgid "Hide boosts from %1"
msgstr "Dölj förstärkningar från %1"

#: content/ui/AccountInfo.qml:134
#, kde-format
msgid "Stop hiding boosts from %1"
msgstr "Sluta dölja förstärkningar från %1"

#: content/ui/AccountInfo.qml:150
#, kde-format
msgid "Stop featuring on profile"
msgstr "Sluta presentera för profil"

#: content/ui/AccountInfo.qml:152
#, kde-format
msgid "Feature on profile"
msgstr "Presentera för profil"

#: content/ui/AccountInfo.qml:168
#, kde-format
msgid "Stop muting"
msgstr "Sluta tysta"

#: content/ui/AccountInfo.qml:170
#, kde-format
msgid "Mute"
msgstr "Tysta"

#: content/ui/AccountInfo.qml:186
#, kde-format
msgid "Stop blocking"
msgstr "Sluta blockera"

#: content/ui/AccountInfo.qml:188
#, kde-format
msgid "Block"
msgstr "Blockera"

#: content/ui/AccountInfo.qml:294
#, kde-format
msgid "Click to add a note"
msgstr "Klicka för att lägga till en anmärkning"

#: content/ui/AccountInfo.qml:360
#, kde-format
msgid "%1 toots"
msgstr "%1 tutor"

#: content/ui/AccountInfo.qml:364
#, kde-format
msgid "%1 followers"
msgstr "%1 följare"

#: content/ui/AccountInfo.qml:369
#, kde-format
msgid "%1 following"
msgstr "%1 följer"

#: content/ui/AuthorizationPage.qml:21
#, kde-format
msgid "To continue, please open the following link and authorize Tokodon: %1"
msgstr "Fortsätt genom att öppna följande länk och godkänna Tokodon:%1"

#: content/ui/AuthorizationPage.qml:37 content/ui/AuthorizationPage.qml:49
#, kde-format
msgid "Copy link"
msgstr "Kopiera länk"

#: content/ui/AuthorizationPage.qml:41 content/ui/AuthorizationPage.qml:48
#, kde-format
msgid "Open link"
msgstr "Öppna länk"

#: content/ui/AuthorizationPage.qml:53
#, kde-format
msgid "Enter token:"
msgstr "Ange token:"

#: content/ui/AuthorizationPage.qml:64 content/ui/LoginPage.qml:36
#, kde-format
msgid "Continue"
msgstr "Fortsätt"

#: content/ui/AuthorizationPage.qml:67
#, kde-format
msgid "Please insert the generated token."
msgstr "Infoga genererade symbol."

#: content/ui/FullScreenImage.qml:18
#, kde-format
msgid "Image View"
msgstr "Bildvy"

#: content/ui/FullScreenImage.qml:84
#, kde-format
msgid "Previous image"
msgstr "Föregående bild"

#: content/ui/FullScreenImage.qml:99
#, kde-format
msgid "Next image"
msgstr "Nästa bild"

#: content/ui/FullScreenImage.qml:109
#, kde-format
msgid "Close"
msgstr "Stäng"

#: content/ui/LoginPage.qml:13
#, kde-format
msgid "Login"
msgstr "Logga in"

#: content/ui/LoginPage.qml:18
#, kde-format
msgid "Welcome to Tokodon"
msgstr "Välkommen till Tokodon"

#: content/ui/LoginPage.qml:22
#, kde-format
msgid "Instance Url:"
msgstr "Instans Url:"

#: content/ui/LoginPage.qml:27
#, kde-format
msgid "Username:"
msgstr "Användarnamn:"

#: content/ui/LoginPage.qml:32
#, kde-format
msgid "Ignore ssl errors"
msgstr "Ignorera ssl-fel"

#: content/ui/LoginPage.qml:39
#, kde-format
msgid "Instance URL and username must not be empty!"
msgstr "Instansens webbadress och användarnamn får inte vara tomma."

#: content/ui/main.qml:96 content/ui/Settings/AccountsCard.qml:90
#, kde-format
msgid "Add Account"
msgstr "Lägg till konto"

#: content/ui/main.qml:118
#, kde-format
msgctxt "@action:inmenu"
msgid "Settings"
msgstr "Inställningar"

#: content/ui/main.qml:126
#, kde-format
msgid "Home"
msgstr "Hem"

#: content/ui/main.qml:137 content/ui/NotificationPage.qml:13
#, kde-format
msgid "Notifications"
msgstr "Underrättelser"

#: content/ui/main.qml:146
#, kde-format
msgid "Local"
msgstr "Lokal"

#: content/ui/main.qml:158
#, kde-format
msgid "Global"
msgstr "Global"

#: content/ui/NotificationPage.qml:22 content/ui/TimelinePage.qml:45
#, kde-format
msgid "Toot"
msgstr "Tut"

#: content/ui/NotificationPage.qml:34
#, kde-format
msgctxt "Show all notifications"
msgid "All"
msgstr "Alla"

#: content/ui/NotificationPage.qml:44
#, kde-format
msgctxt "Show only mentions"
msgid "Mentions"
msgstr "Omnämnanden"

#: content/ui/NotificationPage.qml:92 notificationhandler.cpp:28
#, kde-format
msgid "%1 followed you"
msgstr "%1 följer dig"

#: content/ui/NotificationPage.qml:98 content/ui/TimelinePage.qml:67
#, fuzzy, kde-format
#| msgid "Loading"
msgid "Loading..."
msgstr "Läser in"

#: content/ui/PostDelegate.qml:42 notificationhandler.cpp:24
#, kde-format
msgid "%1 favorited your post"
msgstr "%1 markerade ditt inlägg som favorit"

#: content/ui/PostDelegate.qml:56
#, kde-format
msgid "Pinned entry"
msgstr "Fäst post"

#: content/ui/PostDelegate.qml:74
#, kde-format
msgid "%1 boosted"
msgstr "%1 förstärkte"

#: content/ui/PostDelegate.qml:74 notificationhandler.cpp:32
#, kde-format
msgid "%1 boosted your post"
msgstr "%1 förstärkte ditt inlägg"

#: content/ui/PostDelegate.qml:125
#, kde-format
msgid "Show more"
msgstr "Visa fler"

#: content/ui/PostDelegate.qml:297
#, kde-format
msgctxt "Reply to a post"
msgid "Reply"
msgstr "Svara"

#: content/ui/PostDelegate.qml:307
#, kde-format
msgctxt "Share a post"
msgid "Boost"
msgstr "Förstärk"

#: content/ui/PostDelegate.qml:317
#, kde-format
msgctxt "Like a post"
msgid "Like"
msgstr "Gilla"

#: content/ui/Settings/AccountsCard.qml:22
#, kde-format
msgid "Accounts"
msgstr "Konton"

#: content/ui/Settings/AccountsCard.qml:67
#, kde-format
msgid "Logout"
msgstr "Logga ut"

#: content/ui/Settings/GeneralCard.qml:18
#, kde-format
msgid "General"
msgstr "Allmänt"

#: content/ui/Settings/GeneralCard.qml:23
#, fuzzy, kde-format
#| msgid "Show detailed statistics about posts."
msgid "Show detailed statistics about posts"
msgstr "Visa detaljerad statistik om inlägg."

#: content/ui/Settings/GeneralCard.qml:36
#, fuzzy, kde-format
#| msgid "Show link preview."
msgid "Show link preview"
msgstr "Visa förhandsgranskning av länk."

#: content/ui/Settings/SettingsPage.qml:14
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Settings"
msgctxt "@title:window"
msgid "Settings"
msgstr "Inställningar"

#: content/ui/Settings/SonnetCard.qml:24
#, fuzzy, kde-format
#| msgid "Spell Checking"
msgid "Spellchecking"
msgstr "Stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:30
#, kde-format
msgid "Enable automatic spell checking"
msgstr "Aktivera automatisk stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:42
#, kde-format
msgid "Ignore uppercase words"
msgstr "Ignorera ord med stora bokstäver"

#: content/ui/Settings/SonnetCard.qml:54
#, kde-format
msgid "Ignore hyphenated words"
msgstr "Ignorera ord med bindestreck"

#: content/ui/Settings/SonnetCard.qml:66
#, kde-format
msgid "Detect language automatically"
msgstr "Identifiera automatiskt språk"

#: content/ui/Settings/SonnetCard.qml:77
#, kde-format
msgid "Selected default language:"
msgstr "Valt standardspråk:"

#: content/ui/Settings/SonnetCard.qml:89
#, fuzzy, kde-format
#| msgid "Spell checking languages"
msgid "Additional spell checking languages"
msgstr "Språk för stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:90
#, kde-format
msgid ""
"%1 will provide spell checking and suggestions for the languages listed here "
"when autodetection is enabled."
msgstr ""
"%1 tillhandahåller stavningskontroll och förslag för språken som listas här "
"när automatisk identifiering är aktiverad."

#: content/ui/Settings/SonnetCard.qml:101
#, kde-format
msgid "Open Personal Dictionary"
msgstr "Öppna personlig ordlista"

#: content/ui/Settings/SonnetCard.qml:112
#, fuzzy, kde-format
#| msgid "Spell checking languages"
msgctxt "@title:window"
msgid "Spell checking languages"
msgstr "Språk för stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:122
#: content/ui/Settings/SonnetCard.qml:132
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgid "Default Language"
msgstr "Valt standardspråk:"

#: content/ui/Settings/SonnetCard.qml:144
#, kde-format
msgid "Spell checking dictionary"
msgstr "Ordlista för stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:151
#, kde-format
msgid "Add a new word to your personal dictionary…"
msgstr "Lägg till ett nytt ord i din personliga ordlista…"

#: content/ui/Settings/SonnetCard.qml:154
#, kde-format
msgctxt "@action:button"
msgid "Add word"
msgstr "Lägg till ord"

#: content/ui/Settings/SonnetCard.qml:181
#, kde-format
msgid "Delete word"
msgstr "Ta bort ord"

#: content/ui/TootComposer.qml:13
#, kde-format
msgid "Write a new toot"
msgstr "Skriv en ny tuta"

#: content/ui/TootComposer.qml:20 content/ui/TootComposer.qml:219
#, kde-format
msgid "Content Warning"
msgstr "Innehållsvarning"

#: content/ui/TootComposer.qml:28
#, kde-format
msgid "What's new?"
msgstr "Vad är en nytt?"

#: content/ui/TootComposer.qml:124
#, kde-format
msgid "Make pool auto-exclusive"
msgstr "Gör omröstning automatiskt exklusiv"

#: content/ui/TootComposer.qml:132
#, kde-format
msgid "Choice %1"
msgstr "Val %1"

#: content/ui/TootComposer.qml:156
#, kde-format
msgid "Please choose a file"
msgstr "Välj en fil"

#: content/ui/TootComposer.qml:160
#, kde-format
msgid "Attach File"
msgstr "Bifoga fil"

#: content/ui/TootComposer.qml:168
#, kde-format
msgid "Add Poll"
msgstr "Bifoga enkät"

#: content/ui/TootComposer.qml:191
#, kde-format
msgid "Public"
msgstr "Öppen"

#: content/ui/TootComposer.qml:196
#, kde-format
msgid "Unlisted"
msgstr "Olistad"

#: content/ui/TootComposer.qml:201
#, kde-format
msgid "Private"
msgstr "Privat"

#: content/ui/TootComposer.qml:206
#, kde-format
msgid "Direct Message"
msgstr "Direkt meddelande"

#: content/ui/TootComposer.qml:211
#, kde-format
msgid "Visibility"
msgstr "Synlighet"

#: content/ui/TootComposer.qml:216
#, kde-format
msgctxt "Short for content warning"
msgid "cw"
msgstr "iv"

#: content/ui/TootComposer.qml:228
#, kde-format
msgid "Send"
msgstr "Skicka"

#: main.cpp:81
#, kde-format
msgid "Tokodon"
msgstr "Tokodon"

#: main.cpp:83
#, kde-format
msgid "Mastodon client"
msgstr "Mastodon-klient"

#: main.cpp:85
#, kde-format
msgid "© 2021 Carl Schwan, 2021 KDE Community"
msgstr "© 2021 Carl Schwan, 2021 KDE-gemenskapen"

#: main.cpp:86
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:123
#, kde-format
msgid "Client for the decentralized social network: mastodon"
msgstr "Klient för det decentraliserade sociala nätverket: mastodon"

#: notificationmodel.cpp:94
#, kde-format
msgid "Error occurred when fetching the latest notification."
msgstr ""

#: notificationmodel.cpp:262 timelinemodel.cpp:295
#, kde-format
msgctxt "hour:minute"
msgid "%1:%2"
msgstr "%1:%2"

#: notificationmodel.cpp:266 timelinemodel.cpp:299
#, kde-format
msgid "%1h"
msgstr "%1 t"

#: notificationmodel.cpp:268 timelinemodel.cpp:301
#, kde-format
msgid "%1d"
msgstr "%1 d"

#: threadmodel.cpp:17
#, kde-format
msgctxt "@title"
msgid "Thread"
msgstr "Tråd"

#: timelinemodel.cpp:32
#, kde-format
msgctxt "@title"
msgid "Home (%1)"
msgstr "Hem (%1)"

#: timelinemodel.cpp:34
#, kde-format
msgctxt "@title"
msgid "Home"
msgstr "Hem"

#: timelinemodel.cpp:37
#, kde-format
msgctxt "@title"
msgid "Local Timeline"
msgstr "Lokal tidslinje"

#: timelinemodel.cpp:39
#, kde-format
msgctxt "@title"
msgid "Global Timeline"
msgstr "Global tidslinje"

#~ msgid "Local Timeline"
#~ msgstr "Lokal tidslinje"

#~ msgid "Global Timeline"
#~ msgstr "Global tidslinje"

#~ msgid "Add an account"
#~ msgstr "Lägg till ett konto"

#~ msgid "Options:"
#~ msgstr "Alternativ:"

#~ msgid "Apply"
#~ msgstr "Verkställ"

#~ msgid "Notifications support is not implemented yet"
#~ msgstr "Stöd för underrättelser är inte ännu implementerat"

#~ msgid "Shared by %1"
#~ msgstr "Delas av %1"
